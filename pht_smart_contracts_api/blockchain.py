import iroha

from GLOBALS import host_general as HOST_GENERAL
from GLOBALS import port_general as PORT_GENERAL
from GLOBALS import clave_privada_general as CLAVE_PRIVADA_GENERAL
from GLOBALS import clave_privada_pacientes as CLAVE_PRIVADA_PACIENTES

import iroha_functions.get_EHRs_paciente as iroha_get_EHRs_paciente
import iroha_functions.save_EHR_paciente as iroha_save_EHR_paciente


def get_blocks(patient_host, patient_port, user_name, user_pass):
    return iroha_get_EHRs_paciente.execute(patient_host, patient_port, user_name, CLAVE_PRIVADA_PACIENTES)



def add_block_to_patient_blockchain(patient_host, patient_port, user_name, user_pass, content):

    return iroha_save_EHR_paciente.execute(patient_host, patient_port, user_name, CLAVE_PRIVADA_PACIENTES, content)


