from flask import Flask, g, jsonify, request, abort, Response
from flask_cors import CORS

import blockchain as bc
import resources_storage as resources

import time
import secrets

HOST = "localhost"
PORT = 38384


api = Flask(__name__)
CORS(api)

DIRECTIONS = {}

@api.route("/login", methods=["POST"])
def login():

    try:
        data = request.get_json()
        patient_host = data["host"]
        patient_port = data["port"]

    except:
        return "", 400

    try:
        identifier = secrets.token_hex(4)
        DIRECTIONS[identifier] = {"host": patient_host, "port": patient_port}
    except:
        return "", 500

    return jsonify({"id_session": str(identifier)}), 200



@api.route("/logout/<idsession>", methods=["POST"])
def logout(idsession):
    try:
        del DIRECTIONS[idsession]
    except:
        return "", 500

    return "", 200









@api.route("/<idsession>")
def get_traceability(idsession):


    if request.headers['Authorization']==None or request.headers['Authorization']=="":
        return "", 404

    user_name = request.headers['Authorization'].split("###")[0]
    user_pass = request.headers['Authorization'].split("###")[1]

    if DIRECTIONS[idsession] == None:
        return "", 404


    else:
        try:
            patient_host = DIRECTIONS[idsession]["host"]
            patient_port = DIRECTIONS[idsession]["port"]
        except:
            return "", 404
        result = bc.get_blocks(patient_host, patient_port, user_name, user_pass)
        return jsonify(result)








@api.route("/<idsession>", methods=["POST"])
def create_file(idsession):

    if request.headers['Authorization']==None or request.headers['Authorization']=="":
        return "", 404

    user_name = request.headers['Authorization'].split("###")[0]
    user_pass = request.headers['Authorization'].split("###")[1]

    if DIRECTIONS[idsession] == None:
        return "", 404

    patient_host = DIRECTIONS[idsession]["host"]
    patient_port = DIRECTIONS[idsession]["port"]

    if DIRECTIONS[idsession] == None:
        return "", 404

    result = resources.write_file(request.files['file'], None, None)

    if result == None:
        return "", 500

    content = "C"+"##"+str(result["fileid"])+"##"+str(result["token"])+"##"+str(result["version"])+"##"+str(result["content_hash"])
    result = bc.add_block_to_patient_blockchain(patient_host, patient_port, user_name, user_pass, content)

    if result == True:
        return "", 201
    else:
        return "", 401



@api.route("/<idsession>/<id_file>", methods=["POST"])
def update_file(idsession,id_file):

    if request.headers['Authorization']==None or request.headers['Authorization']=="":
        return "", 404

    user_name = request.headers['Authorization'].split("###")[0]
    user_pass = request.headers['Authorization'].split("###")[1]

    if DIRECTIONS[idsession] == None:
        return "", 404

    patient_host = DIRECTIONS[idsession]["host"]
    patient_port = DIRECTIONS[idsession]["port"]

    if DIRECTIONS[idsession] == None:
        return "", 404

    
    pass_file = request.headers['Authorization'].split("###")[2]
    result = resources.write_file(request.files['file'], id_file, pass_file)

    if result == None:
        return "", 500

    content = "U"+"##"+str(result["fileid"])+"##"+str(result["version"])+"##"+str(result["content_hash"])
    result = bc.add_block_to_patient_blockchain(patient_host, patient_port, user_name, user_pass, content)

    if result == True:
        return "", 201
    else:
        return "", 401



@api.route("/<idsession>/<id_file>", methods=["DELETE"])
def delete_file(idsession,id_file):

    if request.headers['Authorization']==None or request.headers['Authorization']=="":
        return "", 404

    user_name = request.headers['Authorization'].split("###")[0]
    user_pass = request.headers['Authorization'].split("###")[1]

    if DIRECTIONS[idsession] == None:
        return "", 404

    patient_host = DIRECTIONS[idsession]["host"]
    patient_port = DIRECTIONS[idsession]["port"]

    if DIRECTIONS[idsession] == None:
        return "", 404

    
    pass_file = request.headers['Authorization'].split("###")[2]
    result = resources.delete_file(id_file, pass_file)

    if result == False:
        return "", 500

    content = "D##"+str(id_file)
    result = bc.add_block_to_patient_blockchain(patient_host, patient_port, user_name, user_pass, content)

    if result == True:
        return "", 201
    else:
        return "", 401






@api.route("/<idsession>/<id_file>", methods=["GET"])
def read_file(idsession,id_file):
    if request.headers['Authorization']==None or request.headers['Authorization']=="":
        return "", 404

    user_name = request.headers['Authorization'].split("###")[0]
    user_pass = request.headers['Authorization'].split("###")[1]

    if DIRECTIONS[idsession] == None:
        return "", 404

    patient_host = DIRECTIONS[idsession]["host"]
    patient_port = DIRECTIONS[idsession]["port"]

    if DIRECTIONS[idsession] == None:
        return "", 404

    
    pass_file = request.headers['Authorization'].split("###")[2]
    content_disposition, content_type, data = resources.read_file(id_file, pass_file)

    if data == None:
        return "", 500

    if data == False:
        return "", 404
    else:
        content = "R##"+str(id_file)
        result = bc.add_block_to_patient_blockchain(patient_host, patient_port, user_name, user_pass, content)

        if result==True:
            response = Response(data)
            response.headers['Content-Disposition'] = content_disposition
            response.headers['Content-Type'] = content_type

            print(content_disposition)
            print(content_type)
            return response
        else:
            return "",500



if __name__ == "__main__":
    api.run(debug=True, host=HOST, port=PORT)
