from iroha import Iroha, IrohaCrypto, IrohaGrpc
import json
from google.protobuf.json_format import MessageToJson

def find(key, dictionary):
    for k, v in dictionary.items():
        if k == key:
            yield v
        elif isinstance(v, dict):
            for result in find(key, v):
                yield result
        elif isinstance(v, list):
            for d in v:
                for result in find(key, d):
                    yield result


def execute(hostgeneral,portgeneral,claveprivada, idpaciente):

    iroha = Iroha('admin@test')
    net = IrohaGrpc(str(hostgeneral)+':'+str(portgeneral))

    admin_key = claveprivada

    result = net.send_query(
        IrohaCrypto.sign_query(
            iroha.query('GetAccountAssetTransactions',
                        account_id='admin@test',
                        asset_id="paciente"+str(idpaciente)+"#test",
                        page_size=100
                        ),
            admin_key
        )
    )

    json_result = json.loads(MessageToJson(result))

    print(type(json_result))
    descriptions =list(find("description",json_result))
    print(descriptions)

    if len(descriptions) > 0:
        description = descriptions[-1] #return only the last state
        values = description.split("##")
        bc ={
            "host":values[0],
            "port":values[1]
        }
        return True, bc
    else:
        return False, None