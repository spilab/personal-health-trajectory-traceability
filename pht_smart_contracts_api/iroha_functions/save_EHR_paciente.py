from iroha import Iroha, IrohaCrypto, IrohaGrpc

def execute(hostpaciente,portpaciente,username, claveprivada,content):

    iroha = Iroha('admin@test')
    net = IrohaGrpc(str(hostpaciente)+':'+str(portpaciente))

    admin_key = claveprivada
    admin_tx = iroha.transaction(
     [iroha.command(
         'TransferAsset',
         src_account_id='admin@test',
         dest_account_id=str(username),
         asset_id='ehr#test',
         description=str(content),
         amount='1'
     )]
    )

    IrohaCrypto.sign_transaction(admin_tx, admin_key)
    net.send_tx(admin_tx)

    for status in net.tx_status_stream(admin_tx):
        print(status)
        if status[0]=="COMMITTED":
            return True
    return False
