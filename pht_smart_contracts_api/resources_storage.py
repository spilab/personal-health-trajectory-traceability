import requests
import re
import urllib3

RESOURCES_STORAGE_HOST = "http://localhost"
RESOURCES_STORAGE_PORT = 38383

def write_file(data, idfile, passfile):


    if idfile==None:
        #write new file in resources api
        path = str(RESOURCES_STORAGE_HOST)+":"+str(RESOURCES_STORAGE_PORT)+"/files/filename"
        r = requests.post(path, files={'file':data})

    else:
        #update file in reosurces API  mandando como authoruzation la pass
        path = str(RESOURCES_STORAGE_HOST)+":"+str(RESOURCES_STORAGE_PORT)+"/files/filename/"+str(idfile)
        r = requests.post(path, files={'file':data}, headers={"Authorization":passfile})

    response_code = r.status_code
    
    if response_code==500 or response_code==400:
        return None
    else:
        return r.json()





def delete_file(idfile, passfile):


    #delete file in reosurces API  mandando como authoruzation la pass
    path = str(RESOURCES_STORAGE_HOST)+":"+str(RESOURCES_STORAGE_PORT)+"/files/"+str(idfile)
    r = requests.delete(path, headers={"Authorization":passfile})

    response_code = r.status_code
    if response_code==500 or response_code==400:
        return False

    else:
        return True



def read_file(idfile, passfile):


    #get data file in reosurces API mandando como authoruzation la pass
    path = str(RESOURCES_STORAGE_HOST)+":"+str(RESOURCES_STORAGE_PORT)+"/files/"+str(idfile)
    r = requests.get(path, headers={"Authorization":passfile, "Accept":"*/*", "Accept-Encoding":"gzip, deflate, br", "Connection": "keep-alive"})

    response_code = r.status_code
    if response_code==500 or response_code==400:
        return None
    else:
        if response_code ==404:
            return False
        else:
            return r.headers['content-disposition'], r.headers["content-type"], r

