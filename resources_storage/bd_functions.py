import sqlite3
from sqlite3 import Error
import os
import secrets

UPLOAD_DIRECTORY = "resources"
DB_PATH = "bd.sqlite3"


def get_db():
    print("Getting db connection...")
    return sqlite3.connect(DB_PATH)

def close_connection(db):

    print("Closing db connection...")
    if db is not None:
        db.close()


def getResourcePath(fileid, token):
    print("INFO: Getting resource..")
    conn = get_db()
    cur = conn.cursor()
    cur.execute("SELECT * FROM Resources WHERE fileid=? AND token=? ORDER BY version ASC",(fileid,token))

    rows = cur.fetchall()
    close_connection(conn)
    if(len(rows)>1):
        paths = []
        for row in rows:
            paths.append(row[2])
        return paths
    elif(len(rows)==0):
        return None

    row= rows[0]

    return [row[2]]



def getVersion(fileid):
    print("INFO: Getting version for resource..")
    conn = get_db()
    cur = conn.cursor()
    cur.execute("SELECT * FROM Resources WHERE fileid=?",(fileid,))

    rows = cur.fetchall()
    close_connection(conn)
    return len(rows)+1


def removeResource(fileid):
    print("INFO: Removing resource..")
    try:
        conn = get_db()
        cur = conn.cursor()
        cur.execute("DELETE FROM Resources WHERE fileid=?",(fileid))
        conn.commit()
        close_connection(conn)
        return True
    except:
        return False


def writeInResources(fileid,token,path,version):

    conn = get_db()
    cur = conn.cursor()

    try:
        cur.execute("INSERT INTO Resources (fileid,token,path,version)VALUES (?,?,?,?)", [fileid,token,path,version])
        conn.commit()
        close_connection(conn)
        return True
    except Error as e:
        close_connection(conn)
        print(e)
        return False
    except:
        return False


def getContentHash(path):

    return "CASASCDACSDVSFVSDCSDc"
    
def saveResource(filename, data):

    print("INFO: Saving resource..")


    token = generateToken()
    fileid = generateToken()
    version = getVersion(fileid)

    if not os.path.exists(os.path.join(UPLOAD_DIRECTORY, str(fileid))):
        os.mkdir(os.path.join(UPLOAD_DIRECTORY, str(fileid)))
    if not os.path.exists(os.path.join(UPLOAD_DIRECTORY, str(fileid),str(version))):
        os.mkdir(os.path.join(UPLOAD_DIRECTORY, str(fileid),str(version)))

    path = os.path.join(UPLOAD_DIRECTORY,str(fileid),str(version),filename)


    status = writeInResources(fileid,token,path, version)

    if status==True:        
        try:
            data.save(path)
            content_hash = getContentHash(path)
            return True, fileid, token, version, content_hash
        except Error as e:
            print(e)
            return False, "", "", ""

    else:
        return False, "", "", ""


def updateResource(filename,fileid,token, data):

    print("INFO: Saving resource..")

    version = getVersion(fileid)

    if not os.path.exists(os.path.join(UPLOAD_DIRECTORY, str(fileid))):
        os.mkdir(os.path.join(UPLOAD_DIRECTORY, str(fileid)))
    if not os.path.exists(os.path.join(UPLOAD_DIRECTORY, str(fileid),str(version))):
        os.mkdir(os.path.join(UPLOAD_DIRECTORY, str(fileid),str(version)))
    
    path = os.path.join(UPLOAD_DIRECTORY, str(fileid),str(version), filename)


    status = writeInResources(fileid,token,path,version)

    if status==True:        
        try:
            data.save(path)
            content_hash = getContentHash(path)
            return True, fileid, token, version, content_hash
        except Error as e:
            print(e)
            return False, "", "", ""

    else:
        return False, "", "", ""


def generateToken():
    #each byte is 1.3 characters aprox. To get 6 characters we use 4 bytes tokens
    return secrets.token_urlsafe(nbytes=4)
