import os

from flask import Flask, request, abort, jsonify, send_from_directory
from flask_cors import CORS

import bd_functions as BD



HOST = "localhost"
PORT = 38383

if not os.path.exists(BD.UPLOAD_DIRECTORY):
    os.makedirs(BD.UPLOAD_DIRECTORY)


api = Flask(__name__)
CORS(api, expose_headers=["Content-Disposition"])


@api.route("/files/<fileid>")
def get_file(fileid):

    try:
        token = request.headers['Authorization']
    except Exception as e:
        return "",401

    if token == None:
        return 404
    else:
        paths = BD.getResourcePath(fileid,token)
        if paths == None:
            return "",404
        else:
            print("Enviando",str(paths[-1]))
            return send_from_directory("",str(paths[-1]).replace("\\", "/"), as_attachment=True) #cambiar



@api.route("/files/<filename>", methods=["POST"])
def post_file(filename):
    data = request.files['file']
    filename = request.files['file'].filename

    added, fileid, token, version, content_hash= BD.saveResource(filename, data)

    data = {"fileid": fileid,"token": token, "version": version, "content_hash":content_hash}
    if added:
        return jsonify(data), 201
    else:
        return "",500


@api.route("/files/<filename>/<idfile>", methods=["POST"])
def update_file(filename,idfile):
    data = request.files['file']
    filename = request.files['file'].filename
    try:
        token = request.headers['Authorization']
    except Exception as e:
        return "",401

    added, fileid, token, version, content_hash = BD.updateResource(filename,idfile,token,data)

    data = {"fileid": fileid,"token": token, "version": version, "content_hash":content_hash}
    if added:
        return jsonify(data), 201
    else:
        return "",500


@api.route("/files/<idfile>", methods=["DELETE"])
def delete_file(idfile):
    data = request.data

    try:
        token = request.headers['Authorization']
    except Exception as e:
        return "",401

    if token == None:
        return 404
    else:
        paths = BD.getResourcePath(idfile,token)
        if paths == None:
            return "",404
        else:
            for path in paths:
                os.remove(path)
            BD.removeResource(idfile)
            return "", 204


if __name__ == "__main__":
    api.run(debug=True, host=HOST, port=PORT)
